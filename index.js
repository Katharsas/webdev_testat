/*
 * ############# TESTING ##############
 */
var testDayOfWeek;// 1 = Monday
var testTimeString;// "14:20"
/*
 * ########### TESTING END ############
 */



/*
 * module
 */
(function () {
	
	/**
	 * Frequently used DOM nodes.
	 */
	var table = document.getElementById("timetable");
	var details = document.getElementById("details-list");
	
	/**
	 * Currently selected cell from timetable/preview, details are shown for this cell.
	 */
	var selectedNode = null;
	
	var currentDayOfWeek = null;
	
	/**
	 * Maps a short name to every subjects data. Key will be used in HTML, JS
	 * will load data (reduces redundancy).
	 */
	var subjects = {
		webdev : {
			name : "Web Development I",
			docent: "Prof. Dr. Andrej Bachmann"
		},
		pp : {
			name : "Parallele Programmierung",
			docent : "Prof. Dr. Günther Köhler"
		},
		eng : {
			name : "Englisch für Informatiker",
			docent : "Kurt Klingler"
		},
		cpp : {
			name : "Effizientes Programmieren C/C++",
			docent : "Prof. Dr. Peter Stöhr"
		},
		swe2 : {
			name : "Software Engineering II",
			docent : "Prof. Dr. Martin Thost"
		}
	};
	
	
	var removeClass = function(element, clazz) {
		element.classList.remove(clazz);
	};
	var addClass = function(element, clazz) {
		if (!element.classList.contains(clazz)) {
			element.classList.add(clazz);
		}
	};
	
	var getTimeOfCell = function(cell) {
		//time from first column
		return cell.parentNode.children[0].textContent;
	};
	
	/**
	 * Handles click action onto timetable cells
	 */
	var onclickHandler = function(cell, subject, hasTime) {
		if (cell === selectedNode) {
			selectedNode = null;//unselect & hide
			details.setAttribute("style", "display:none");
			removeClass(cell, "selected");
		} else {
			if (selectedNode !== null) removeClass(selectedNode, "selected");
			selectedNode = cell;//select & show
			details.removeAttribute("style");
			addClass(cell, "selected");
			//name
			var nameNode = document.getElementById("detail-name");
			nameNode.textContent = subject.name;
			//docent
			var docentNode = document.getElementById("detail-docent");
			docentNode.textContent = subject.docent;
			//room
			var roomNode = document.getElementById("detail-room");
			roomNode.textContent = cell.children[1].textContent;
			//time
			var timeNode = document.getElementById("detail-time");
			timeNode.textContent = hasTime ? getTimeOfCell(cell) : "---";
			//type
			var typeNode = document.getElementById("detail-type");
			typeNode.textContent = cell.classList.contains("practice") ?
					"Übung" : "Vorlesung";
		}
	};
	
	/**
	 * Get all timetable cells of a day/column.
	 * @param {Number} dayOfWeek - Integer in range [1,4] for days of week: Monday to Thursday
	 * @param {Boolean} filled - If true, only filled cells will be returned,
	 * if false only empty cells, if null both filled & empty cells.
	 * @return {Element[]} - Array of all cell nodes in timetable for given day column
	 */
	var getCellsForDay = function(dayOfWeek, filled) {
		var result = [];
		var rows = table.children;
		//ditch the first row because its the description row
		for (var i = 1; i < rows.length; i++) {
			var row = rows[i];
			//ditch clearfixes
			if (!row.classList.contains("clear")) {
				var cell = (row.children)[dayOfWeek];
				var cellFilled = cell.children.length > 0;
				if (filled === null || cellFilled === filled) result.push(cell);
			}
		}
		return result;
	};
	
	/**
	 * Get all timetable cells.
	 * @see function getCellsForDay
	 */
	var getCells = function(filled) {
		var result = [];
		// monday to thursday
		for (var i = 1; i <=4; i++) {
			var cells = getCellsForDay(i, filled);
			result = result.concat(cells);
		}
		return result;
	};
	
	/**
	 * Adds full subject name to the cell, adds little "Ü" signs if practice cell,
	 * adds event handler to cell.
	 * @see function initCell
	 */
	var initSubjectCell = function (cell, subject, hasTime) {
		//add "Ü" sign to practice cells
		if (cell.classList.contains("practice")) {
			var pDiv = document.createElement('div');
			pDiv.className = "sign";
			pDiv.textContent = "Ü";
			cell.appendChild(pDiv);
		}
		//add subject data to cells
		var nameDiv = document.createElement('div');
		nameDiv.className = "cell-name";
		nameDiv.textContent = subject.name;
		cell.insertBefore(nameDiv, cell.firstChild);
		cell.addEventListener("click", function(){
			onclickHandler(cell, subject, hasTime);
		});
	};
	
	/**
	 * Populate a timetable cell with data from subjects depending on the cells
	 * class. Also add event listener for the cell. Cells which have a subject
	 * class also are expected to have a room child node and a "lecture"/"practice"
	 * class.
	 * 
 	 * @param {Element} cell - DOM Node
	 */
	var initCell = function(cell, hasTime) {
		var keyFound = false;
		for (var key in subjects) {
			if (cell.classList.contains(key)) {
				keyFound = true;
				initSubjectCell(cell, subjects[key], hasTime);
			}
		}
		// set classes to be able to style empty cell seperatly
		if (keyFound) {
			cell.className = "cell "+cell.className;
		} else {
			cell.className = "cell cell-empty";
		}
	};

	/**
	 * Copy data from subjects into table cells
	 */
	var initTimetable = function() {
		var cells = getCells(null);
		for (var i = 0; i < cells.length; i++) initCell(cells[i], true);
	};
	
	/**
	 * Add options for subject selection.
	 */
	var initFormSubject = function() {
		var select = document.getElementById("form-select-subject");
		for (var key in subjects) {
			var subject = subjects[key];
			var option = document.createElement("option");
			option.value = key;
			option.textContent = subject.name;
			select.appendChild(option);
		}
	};
	
	/**
	 * Populate the form preview with subject data similar to timetable cells.
	 */
	var initPreview = function() {
		var preview = document.getElementById("preview");
		if (preview !== null) initCell(preview, false);
	};
	
	/**
	 * Create date/time elements, highlight current day and grey out cells in the past.
	 */
	var initTimeStuff = function() {
		
		/**
		 * format numbers to always display 2 digits
		 */
		var f = function(dateTimeNumber) {
			return (dateTimeNumber < 10 ? "0" : "") + dateTimeNumber;
		};
		
		/**
		 * Get description node for column (day of week)
		 * @param {Number} dayOfWeek - Integer in range [1,4] for days of week: Monday to Thursday
		 */
		var getDayNode = function(dayOfWeek) {
			if (dayOfWeek >= 1 && dayOfWeek <= 4) {
				var days = (table.children)[0].children;
				return days[dayOfWeek];
			} else return null;
		};
		
		var timeNode = document.getElementById("time");
		var dateNode = document.getElementById("date");
		
		/**
		 * Sets date and time div text
		 */
		var updateDateTime = function() {
			var now = new Date();
			var dateString = f(now.getDate()) + "-" + f(now.getMonth()) + "-" + now.getFullYear();
			var timeString = f(now.getHours()) + ":" + f(now.getMinutes()) + ":" + f(now.getSeconds());
			timeNode.textContent = timeString;
			dateNode.textContent = dateString;
		};
		
		/**
		 * Highlight the timetable day of week if it equals current day. Remove highlighting from
		 * old column if the day changed. Undo greying out when day switches to monday.
		 */
		var updateCurrentDay = function() {
			var dayOfWeek = (new Date()).getDay();
			if (testDayOfWeek !== undefined) dayOfWeek = testDayOfWeek;
			if (currentDayOfWeek !== dayOfWeek) {
				console.log("Day changed!");
				if (currentDayOfWeek !== null) {
					removeClass(getDayNode(currentDayOfWeek), "today");
					//undo greying out on monday
					if (dayOfWeek === 1) {
						var cells = getCellsForDay(currentDayOfWeek, true);
						for (var i = 0; i < cells.length; i++) {
							removeClass(cells[i], "past");
						}
					}
				}
				var newNode = getDayNode(dayOfWeek);
				if (newNode !== null) {
					addClass(newNode, "today");
				}
				currentDayOfWeek = dayOfWeek;
			}
		};
		
		/**
		 * Grey out additional cells if their time is in the past. Does not undo greying out.
		 * @see function updateCurrentDay
		 */
		var updateGreyedOutCells = function() {
			var now = new Date();
			var dayOfWeek = now.getDay();
			if (testDayOfWeek !== undefined) dayOfWeek = testDayOfWeek;
			var timeString =  f(now.getHours()) + ":" + f(now.getMinutes());
			if (testTimeString !== undefined) timeString = testTimeString;
			var pastCells = [];
			//1. add subject cells of all past days
			for (var j = 1; j < dayOfWeek; j++) {
				pastCells = pastCells.concat(getCellsForDay(j, true));
			}
			// 2. get subject cells of today
			var cells = getCellsForDay(dayOfWeek, true);
			for (var i = 0; i < cells.length; i++) {
				var cell = cells[i];
				var subjectEnd = getTimeOfCell(cell).split("-")[1];
				// 3. add if time is lower
				if (subjectEnd < timeString) {
					pastCells.push(cell);
				}
			}
			// 4. grey out cells
			for (var k = 0; k < pastCells.length; k++) {
				addClass(pastCells[k], "past");
			}
		};
		
		setInterval(function() {
			updateDateTime();
			updateCurrentDay();
			updateGreyedOutCells();
		}, 500);
	};
	
	/* 
	 * ###################################
	 * #------------- START -------------#
	 * ###################################
	 */
	initTimetable();
	initFormSubject();
	initPreview();
	initTimeStuff();
})();

