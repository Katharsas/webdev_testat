<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title>index</title>
		<link rel="stylesheet" href="css/960_16_col.css"/>
		<link rel="stylesheet" href="css/index.css"/>
		<link rel="stylesheet" href="css/details.css"/>
		<link rel="stylesheet" href="css/timetable.css"/>
	</head>

	<body>
		<div id="main" class="container_16" style="background-color:#6A6">
			<div class="grid_2 brightgreen">Zeit: <span id="time"></span></div>
			<div class="grid_2 brightgreen">Datum: <span id="date"></span></div>
			<div class="grid_4 brightgreen">Vorlesungszeit: 01.10.2015 - 22.01.2016</div>
			<div class="grid_4 brightgreen">Prüfungsanmeldung: 11.11.2015 - 19.11.2015</div>
			<div class="grid_4 brightgreen">Weihnachtsferien: 24.12.2015 - 06.01.2016</div>
			<div class="clear"></div>
			<br/>
			<div class="grid_16 brightgreen">Stundenplan</div>
			<div class="clear"></div>
			<div id="timetable">
				<div class="row push_1 col-description">
					<div>Zeit\Tag</div>
					<div>Montag</div>
					<div>Dienstag</div>
					<div>Mittwoch</div>
					<div>Donnerstag</div>
				</div>
				<div class="clear"></div>
				<div class="row push_1">
					<div class="row-time">09:45-11:15</div>
					
					<div></div>
					<div></div>
					<div class="lecture cpp">		<div class="cell-room">FA011</div></div>
					<div class="practice swe2">		<div class="cell-room">FA008</div></div>
				</div>
				<div class="clear"></div>
				<div class="row push_1">
					<div class="row-time">11:30-13:00</div>
					
					<div class="lecture webdev">	<div class="cell-room">FA013</div></div>
					<div class="lecture eng">		<div class="cell-room">FB005</div></div>
					<div class="practice webdev">	<div class="cell-room">FB004A</div></div>
					<div class="lecture eng">		<div class="cell-room">FB005</div></div>
				</div>
				<div class="clear"></div>
				<div class="row push_1">
					<div class="row-time">14:00-15:30</div>
					
					<div></div>
					<div class="lecture pp">		<div class="cell-room">FB006</div></div>
					<div></div>
					<div class="lecture swe2">		<div class="cell-room">FA009</div></div>
				</div>
				<div class="clear"></div>
				<div class="row push_1">
					<div class="row-time">15:45-17:15</div>
					
					<div class="lecture pp">		<div class="cell-room">FB001/2</div></div>
					<div></div>
					<div></div>
					<div class="practice cpp">		<div class="cell-room">FB004b</div></div>
				</div>
				<div class="clear"></div>
			</div>
			<br/>
			<div class="grid_8 brightgreen">
				<div class="h3">Details</div>
				<div id="details-list" style="display:none">
					<div>Name:</div><div class="detail" id="detail-name"> </div>
					<div>Typ:</div><div class="detail" id="detail-type"> </div>
					<div>Dozent:</div><div class="detail "id="detail-docent"> </div>
					<div>Raum:</div><div class="detail" id="detail-room"> </div>
					<div>Zeit:</div><div class="detail" id="detail-time"> </div>
				</div>
				<div class="clear"></div>
			</div>
			<div class="grid_7 push_1 brightgreen">
				<div class="h3">Neuer Eintrag</div>
				<div class="form-container">
					<form id="form" action="#" method="POST">
						<div>Fach:</div>
						<select id="form-select-subject" name="subject">
						</select><br/>
						<label><input type="radio" name="type" value="lecture" checked>Vorlesung</input></label>
						<label><input type="radio" name="type" value="practice">Übung</input></label>
						<div>Raum:</div>
						<input type="text" name="room" value="raum"/><br/>
						<input type="submit"/>
					</form>
					<br/>
					<div class="h3">Vorschau</div>
					<?php
						if($_POST) {
							$room = $_POST["room"];
							$roomHTML = "<div class='cell-room'>$room</div>";
							$subject = $_POST["subject"];
							$type = $_POST["type"];
							$cell = "<div id='preview' class='$type $subject'>$roomHTML</div>";
							echo($cell);
						}
					?>
				</div>
			</div>
			<div class="clear"></div>
		</div>
	</body>
</html>

<script src="index.js"></script>
